
public class Aufgabe_01 
{
	public static void firstCharacters()
	{
		System.out.printf("%5s\n", "**");	
	}
	
	public static void secondCharacters()
	{
		for (int i = 0; i < 2; i++)
		{
			System.out.printf("%s%7s\n", "*", "*");	
		}
	}
	
	
	public static void main(String[] args) 
	{
		firstCharacters();
		secondCharacters();
		firstCharacters();
	}
}